#pragma once
#include <string>
#include <stdint.h>
class Neuron
{
    public:

        Neuron()
        {}
        Neuron (std::string _name,uint16_t _row, double _value)
        {
            name = _name;
            row = _row;
            value = _value;
        }

        Neuron (std::string _name,uint16_t _row)
        {
            name = _name;
            row = _row;
            value = 0;
        }

        void setName(std::string _name)
        {
            name = _name;
        }

        std::string getName()
        {
            return name;
        }

        void setRow(uint16_t _row)
        {
            row = _row;
        }

        uint16_t getRow()
        {
            return row;
        }

        void setValue(double _value)
        {
            value = _value;
        }

        double getValue()
        {
            return value;
        }

        double getDelta()
        {
            return delta;
        }

        double setDelta(double _delta)
        {
            delta = _delta;
        }

    private:

        std::string name;

        uint16_t row;

        double value;

        double delta;


};