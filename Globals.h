#include <vector>

#include"Neuron.h"
#include"Sinaps.h"
class Globals
{


    public:


        std::vector<Neuron> neuronArray;
        std::vector<Sinaps> sinapsArray;
        Globals()
        {
            neuronArray.push_back(Neuron("I1", 0));
            neuronArray.push_back(Neuron("I2", 0));
            neuronArray.push_back(Neuron("H1", 1));
            neuronArray.push_back(Neuron("H2", 1));
            neuronArray.push_back(Neuron("O1", 2));

            sinapsArray.push_back(Sinaps("w1", 0, 2, 0.45));
            sinapsArray.push_back(Sinaps("w2", 0, 3, 0.78));
            sinapsArray.push_back(Sinaps("w3", 1, 2, -0.12));
            sinapsArray.push_back(Sinaps("w4", 1, 3, 0.13));
            sinapsArray.push_back(Sinaps("w5", 2, 4, 1.5));
            sinapsArray.push_back(Sinaps("w6", 3, 4, -2.3));
        }

};