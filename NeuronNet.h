#include <iostream>
#include <cstdlib> // для system
#include <string>
#include <string.h>
#include <cmath>
#include <vector>

#include"Neuron.h"
#include"Sinaps.h"
#include"Loger.h"
#include"Globals.h"
class NeuronNet
{
    public:

        double alfa = 0.5, epsilon = 0.3;
        Neuron* neuronArray[5];
        Sinaps* sinapsArray[6];


        Loger loger = Loger();

        Globals globals = Globals();

                /****NEURON NET*****/




        //Запустить нейронную сеть
        double runNeuron()
        {
            //printf("RUN NEURON\n");
            uint16_t rows = countRows();
            for(uint16_t i = 1; i< rows; i++)
            {
                //printf("Counting row %d\n", i);
                std::vector<uint16_t> neurons = getNeuronsFromRow(i);
                for(uint16_t j =0; j<neurons.size(); j++)
                {
                    
                    std::vector<uint16_t> inSnapses = getInSinapses(neurons[j]);
                    double value = countNeuronValue(inSnapses);
                    globals.neuronArray[neurons[j]].setValue(value);
                    //printf("Neuron %s value %f\n",globals.neuronArray[neurons[j]].getName().c_str(),
                     //globals.neuronArray[neurons[j]].getValue() );
                    if(i == rows - 1)
                    {
                        return value;
                    }//if
                }//for
            }//for
            return NULL;
        }


        void teachNeuronNetItirarion(double outIdeal, double outActual)
        {
            //printf("TEACH NEURON NET ITIRATION\n");
            uint16_t outNeuron = getOutputNeuron();
            globals.neuronArray[outNeuron].setDelta(sigmaOutput(outIdeal, outActual));

            //printf("Out neuron ( %s) delta: %f\n", globals.neuronArray[outNeuron].getName().c_str(),
             //globals.neuronArray[outNeuron].getDelta());

            teachInNeuronSinapses(outNeuron);
           
            for(uint16_t i = 0; i<globals.neuronArray[outNeuron].getRow()-1; i++)
            {
                //printf("Row: %d\n", (globals.neuronArray[outNeuron].getRow()-1)-i);
                std::vector<uint16_t> neurons = getNeuronsFromRow((globals.neuronArray[outNeuron].getRow()-1)-i);
                for(uint16_t j =0; j<neurons.size(); j++)
                {
                    //printf("Neuron sinaps study %s\n", globals.neuronArray[neurons[j]].getName().c_str() );
                    teachInNeuronSinapses(neurons[j]);
                }
            }
        }



    private:

        void teachInNeuronSinapses(uint16_t neuron)
        {
            std::vector<uint16_t> neuronSinapsies = getInSinapses(neuron);
            //printf("CPP after2\n");
            for(uint16_t i =0; i< neuronSinapsies.size(); i++)
            {
                uint16_t n = globals.sinapsArray[neuronSinapsies[i]].getNeuronIn();
                globals.neuronArray[n].setDelta(sigmaHidden(n));
                double grad = globals.neuronArray[n].getValue() * globals.neuronArray[globals.sinapsArray[neuronSinapsies[i]].getNeuronOut()].getDelta();
                double dw = mor(grad,  globals.sinapsArray[neuronSinapsies[i]].getDeltaValueLast());
                globals.sinapsArray[neuronSinapsies[i]].setDeltaValueLast(dw);
                globals.sinapsArray[neuronSinapsies[i]].setValue(globals.sinapsArray[neuronSinapsies[i]].getValue()+dw);
                //printf("Sinaps (%s) grad %f dw %f value %f\n", globals.sinapsArray[neuronSinapsies[i]].getName().c_str(), grad, dw, globals.sinapsArray[neuronSinapsies[i]].getValue());
            }
        }



        /***CASH****/
        int16_t outNeuronCashIndex = -1;
        int16_t rowsCash = 0;
        

        /*******МАТЕМАТИКА ОБУЧЕНИЕ НЕЙРОСЕТИ*******/
        //     *Производная функции активации
        double fSigmoid(double out)
        {
            return (1.0- out)*out;
        }
        //Расчет дельты для выходного нейрона
        double fTangh(double out)
        {
            return 1.0 - out *out;
        }
        //Расчет дельты для выходного нейрона
        double sigmaOutput(double outIdeal, double outActual)
        {
            return (outIdeal - outActual)*fSigmoid(outActual);
        }
        //
        double grad (double a, double b)
        {
            return a * b;
        }
        //Расчет дельты для скрытого нейрона
        double sigmaHidden(uint16_t neuron)
        {
            //uint16_t *outSinapsesLenth = 0;
            std::vector<uint16_t> outSinapses = getOutSinapses(neuron);
            double _fSigmoid = fSigmoid(globals.neuronArray[neuron].getValue());
            double sum =0;
            for(uint16_t i=0; i<outSinapses.size(); i++)
            {
                sum =sum + globals.sinapsArray[outSinapses[i]].getValue()*
                globals.neuronArray[globals.sinapsArray[outSinapses[i]].getNeuronOut()].getDelta();
            }
            return _fSigmoid*sum;
        }


        double mor(double grad, double dw)
        {
            return epsilon*grad + alfa*dw;
        }

         /*******МАТЕМАТИКА РАСЧЕТ ПО НЕЙРОСЕТИ*******/
        double activationFunction(double inValue)
        {
            return 1.0/(1.0+exp(-inValue));
        }

        double countNeuronValue(std::vector<uint16_t> inSinapses)
        {
            double h1 = 0;
            for(uint16_t i = 0; i < inSinapses.size(); i++)
            {
                //printf("Neuron value: %f , neuron name %s\n",
                //globals.neuronArray[globals.sinapsArray[inSinapses[i]].getNeuronIn()].getValue(),
                //globals.neuronArray[globals.sinapsArray[inSinapses[i]].getNeuronIn()].getName().c_str());
                h1 = h1 + globals.sinapsArray[inSinapses[i]].getValue() * 
                globals.neuronArray[globals.sinapsArray[inSinapses[i]].getNeuronIn()].getValue();
            }
            double hOutput = activationFunction(h1);
            return hOutput;
        }

        /***************РЯДЫ НЕЙРОСЕТИ***************/
                //Получить количество рядов в нейросети
        uint16_t countRows()
        {
            if(rowsCash == 0)
            {
                uint16_t rows = 0;
                for(uint16_t i =0; i<globals.neuronArray.size(); i++)
                {
                    if(rows<globals.neuronArray[i].getRow())
                    {
                        rows = globals.neuronArray[i].getRow();
                    }//if
                    rowsCash = rows +1;
                }//for
            }//if

            return rowsCash;
        }
        //Получить нейроны находящиеся в выбраного ряда
        std::vector<uint16_t> getNeuronsFromRow(uint16_t row)
        {
            uint16_t j=0;
            std::vector<uint16_t> outArray;
            for(int i= 0; i<globals.neuronArray.size(); i++)
            {
                if(globals.neuronArray[i].getRow() == row)
                {
                    outArray.push_back(i);
                    j++;                 
                }
            }
            return outArray;
        }
        //
        uint16_t getOutputNeuron()
        {
            if( outNeuronCashIndex<0)
            {
                uint16_t rows = countRows();
                std::vector<uint16_t> neurons = getNeuronsFromRow(rows - 1);
                uint16_t *neuronsLenth = 0;
                outNeuronCashIndex = neurons[0];
            }
            return outNeuronCashIndex;
        }


         /*********РАБОТА С МАССИВОМ СИНАПСОВ*********/

        //Получить входные синапсы нейрона
        std::vector<uint16_t> getInSinapses(uint16_t neuron)
        {
            uint16_t j =0;
            std::vector<uint16_t> outArray;
            
            for(int i= 0; i<globals.sinapsArray.size(); i++)
            {
                
                std::string name1 = globals.neuronArray[globals.sinapsArray[i].getNeuronOut()].getName();
                std::string name2 = globals.neuronArray[neuron].getName();
                
                if(strcmp(name1.c_str(),name2.c_str())==0)
                {
                    outArray.push_back( i);
                    //printf("Sinaps: %s aded\n", sinapsArray[i].getName().c_str());
                    j++;
                }
            }
            return outArray;
        }

       //Получить выходные синапсы нейрона
        std::vector<uint16_t> getOutSinapses(uint16_t neuron)
        {
            std::vector<uint16_t> outArray;
            uint16_t j =0;
            for(int i= 0; i<globals.sinapsArray.size(); i++)
            {
                if(strcmp(globals.neuronArray[globals.sinapsArray[i].getNeuronIn()].getName().c_str(),
                 globals.neuronArray[neuron].getName().c_str())==0)
                {
                    outArray.push_back(i);
                    j++;
                }
            }
            return outArray;
        }

};