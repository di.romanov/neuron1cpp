#include <iostream>
#include <cstdlib> // для system
#include <time.h>

#include"NeuronNet.h"
#include"Neuron.h"
#include"Sinaps.h"


using namespace std;

class ResultTableLine
{
    public:
        ResultTableLine()
        {}
        ResultTableLine(double _a, double _b, double _c)
        {
            a = _a;
            b = _b;
            c = _c;
        }

        double a = 0, b= 0, c=0, result = 0;
};

std::vector<ResultTableLine> xorResultTable(4);

double delta = 0.01;

NeuronNet nn = NeuronNet();



void printlnItiration(uint32_t i, int64_t delta)
{
    printf("Itiration: %d ", i);
    printf("W: %f H: %f W: %f H: %f W: %f H: %f W: %f H: %f ",
    xorResultTable[0].c, xorResultTable[0].result, xorResultTable[1].c, xorResultTable[1].result, 
    xorResultTable[2].c, xorResultTable[2].result, xorResultTable[3].c, xorResultTable[3].result);
    printf("deltaTime: %d\n", delta);

}

bool offset()
{
        for(uint8_t i=0; i<4; i++)
        {
            if(xorResultTable[i].c < xorResultTable[i].result -delta ||
            xorResultTable[i].c > xorResultTable[i].result+delta)
            return false;
        }
        return true;
}

int main() 
{ 


    xorResultTable[0] = ResultTableLine(0,0,0);
    xorResultTable[1] = ResultTableLine(0,1,1);
    xorResultTable[2] = ResultTableLine(1,0,1);
    xorResultTable[3] = ResultTableLine(1,1,0);

    printf("TEST FROM EXAMPLE\n");
    nn.globals.neuronArray[0].setValue(1.0);//i1
    nn.globals.neuronArray[1].setValue(0.0);//i2

    double ans = nn.runNeuron();
    printf("First anser: %f\n", ans);

    nn.teachNeuronNetItirarion(1.0, ans);

    ans = nn.runNeuron();
    printf("Second anser: %f\n", ans);
    printf("RUNING STUDY\n");

    uint32_t i =0;

    long startTime = clock()/(CLOCKS_PER_SEC/1000);
    long previous = clock()/(CLOCKS_PER_SEC/1000);
    while(true)
    {
        
        if(i%1000000 == 0 && i!=0)
        {
            nn.loger.on();
        }
        for(uint8_t i=0; i<4; i++)
        {
            nn.globals.neuronArray[0].setValue(xorResultTable[i].a);//i1
            nn.globals.neuronArray[1].setValue(xorResultTable[i].b);//i2
            double ans = nn.runNeuron();
            nn.teachNeuronNetItirarion(xorResultTable[i].c, ans);
            xorResultTable[i].result = ans;
        }
        if(i%100000 == 0)
        {
            long current  = clock()/(CLOCKS_PER_SEC/1000);
            long delta = current - previous;
            previous = 0;
            previous = current;
            printlnItiration(i, delta);
            nn.loger.off();
            if(offset())
                break;
        }
        i++;
    }
    uint64_t stopTime = clock()/(CLOCKS_PER_SEC/1000);
    uint64_t deltaTime = stopTime - startTime;
    printf("Neuron teached.\nStudy time [s]: %d\nStudy time [ms]: %d\n", deltaTime/1000, deltaTime);
    return 0; 
}

