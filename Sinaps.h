#pragma once
#include <string>
#include <stdint.h>

#include "Neuron.h"
class Sinaps
{

    public:

        Sinaps(std::string _name, uint16_t _neuronInIndex, uint16_t _neuronOutIndex, double _value) 
        {
            name = _name;
            value = _value;
            neuronInIndex = _neuronInIndex;
            neuronOutIndex = _neuronOutIndex;
        }

        Sinaps(std::string _name, uint16_t _neuronInIndex, uint16_t _neuronOutIndex) 
        {
            name = _name;
            neuronInIndex = _neuronInIndex;
            //value = 0;
        }
        Sinaps(){}

        void setName(std::string _name)
        {
            name = _name;
        }

        std::string getName()
        {
            return name;
        }

        void setValue(double _value)
        {
            value = _value;
        }

        double getValue()
        {
            return value;
        }

        void setDeltaValueLast(double _deltaValueLast)
        {
            deltaValueLast = _deltaValueLast;
        }

        double getDeltaValueLast()
        {
            return deltaValueLast;
        }

        void setNeuronIn(uint16_t _neuronInIndex)
        {
            neuronInIndex = _neuronInIndex;
        }


        uint16_t getNeuronIn()
        {
            return neuronInIndex;
        }


        void setNeuronOut(uint16_t _neuronOutIndex)
        {
            neuronOutIndex = _neuronOutIndex;
        }

        uint16_t getNeuronOut()
        {
            return neuronOutIndex;
        }


    private:

        std::string name;

        double deltaValueLast = 0;

        double value;

        uint16_t neuronInIndex;

        uint16_t neuronOutIndex;
};